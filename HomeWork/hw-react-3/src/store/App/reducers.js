import {SAVE_ITEMS, ITEMS_LOADED, CHANGE_CART, CHANGE_FAVORITES} from './actions';

const defaultState = {
    items: {
        itemsList: [],
        loaded: false
    },
    cart: [],
    favorites: [],
    isOpen: false,
}

export const appReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SAVE_ITEMS:
            return {...state, items: {...state.items, itemsList: action.payload}};
        case ITEMS_LOADED:
            return {...state, items: {...state.items, loaded: action.payload}};
        case CHANGE_CART:
            return {...state, cart: action.payload};
        case CHANGE_FAVORITES:
            return {...state, favorites: action.payload};
        default:
            return state;
    }
}