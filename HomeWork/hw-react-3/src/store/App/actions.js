export const SAVE_ITEMS = 'SAVE_ITEMS';
export const ITEMS_LOADED = 'ITEMS_LOADED';
export const CHANGE_CART = 'CHANGE_CART';
export const CHANGE_FAVORITES = 'CHANGE_FAVORITES';

export const saveItems = (items) => ({
    type: SAVE_ITEMS,
    payload: items,
})
export const itemsLoaded = (loaded) => ({
    type: ITEMS_LOADED,
    payload: loaded,
})
export const changeCart = (newCart) => ({
    type: CHANGE_CART,
    payload: newCart,
})
export const changeFavorites = (newFav) => ({
    type: CHANGE_FAVORITES,
    payload: newFav,
})