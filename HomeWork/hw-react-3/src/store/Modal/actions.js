export const IS_MODAL_OPEN = "IS_MODAL_OPEN";
export const SET_MODAL_TITLE = "SET_MODAL_TITLE";
export const SET_MODAL_ID = "SET_MODAL_ID";
export const IS_IN_CART = 'IN_CART'

export const setOpenModal = (isOpen) => ({
    type: IS_MODAL_OPEN,
    payload: isOpen,
});
export const modalTitle = (title) => ({
    type: SET_MODAL_TITLE,
    payload: title,
});
export const modalId = (id) => ({
    type: SET_MODAL_ID,
    payload: id,
});
export const isInCart = (inCart) => ({
    type: IS_IN_CART,
    payload: inCart,
});
