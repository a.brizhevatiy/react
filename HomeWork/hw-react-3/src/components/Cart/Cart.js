import React from 'react';
import PropTypes from "prop-types";

function Cart({cart}) {
    return (
        <div>
            В Корзине: {cart.length}
        </div>
    );
}

Cart.propTypes = {
    cart: PropTypes.array,
};

Cart.defaultProps = {
    cart: [],
};

export default Cart;