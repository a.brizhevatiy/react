import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

function Button({text, onClick, backgroundColor, className}) {
    const style = {
        backgroundColor: backgroundColor,
    }

    return (
        <button className={className} style={style} onClick={onClick}>{text}</button>
    );
}

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
};

Button.defaultProps = {
    text: '',
    onClick: () => {
    },
    className: '',
    backgroundColor: '',
};

export default Button;