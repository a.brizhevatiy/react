import React from 'react';
import Card from "../Card/Card";
import PropTypes from "prop-types";
import "./Cards.scss"

function Cards({items, addToCart, removeFromCart, toFavorites, cart, favorites}) {
    const itemCards = items.map(i => <Card key={i.id}
                                           item={i}
                                           id={i.id}
                                           toFavorites={toFavorites}
                                           addToCart={addToCart}
                                           removeFromCart={removeFromCart}
                                           cart={cart}
                                           favorites={favorites}
                                           inCart={isInArr(i.id, cart)}
                                           inFavorites={isInArr(i.id, favorites)}/>);

    return (
        <>
            {itemCards}
        </>
    );
}

function isInArr(id, arr) {
    return arr.indexOf(id) !== -1;
}

Cards.propTypes = {
    items: PropTypes.array,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    toFavorites: PropTypes.func,
    cart: PropTypes.array,
    favorites: PropTypes.array,
};

Cards.defaultProps = {
    items: [],
    addToCart: () => {
    },
    removeFromCart: () => {
    },
    toFavorites: () => {
    },
    cart: [],
    favorites: [],
};

export default Cards;