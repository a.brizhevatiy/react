import React, {useState} from 'react';
import Image from "../Image/Image";
import Star from "../Star/Star";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Price from "../Price/Price";
import PropTypes from "prop-types";
import "./Card.scss"

function Card({item, inCart, inFavorites, id, toFavorites, addToCart, removeFromCart}) {
    let [modalIsOpen, setOpenModal] = useState(false)

    const {title, price, img,} = item;
    const buttonText = !inCart ? 'В корзину' : 'Из корзины';
    const backgroundColor = buttonText === 'Из корзины' ? 'rgba(255, 0, 0, 0.1)' : 'rgba(0, 0, 0, 0.2)';
    return (
        <>
            <div className='card'>
                <h3 className="card__title">{title} <Star inFavorites={inFavorites}
                                                          onClick={favoriteClick}/>
                </h3>
                <div className="card__image">
                    <Image src={img}/>
                </div>
                <div className='card__price'><Price price={price}/></div>

                {/*!--------------------------!*/}
                {/*<div>Color: {color}</div> а хз к чему его но в тз просили ))))))*/}
                {/*!--------------------------!*/}

                <div>
                    <Button text={buttonText}
                            onClick={openModal}
                            className={'button'}
                            backgroundColor={backgroundColor}/>
                </div>
            </div>
            {!inCart && <Modal header={'Добавление товара в корзину:'}
                               closeButton={true}
                               text={`Вы хотите добавить ${title} в корзину?`}
                               isOpen={modalIsOpen}
                               closeFunc={cancelButtonFunction}>
                <Button text={'OK'}
                        onClick={addingToCart}
                        className={'button'}
                        backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                <Button text={'Cancel'}
                        onClick={cancelButtonFunction}
                        className={'button'}
                        backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
            </Modal>}
            {inCart && <Modal header={'Удаление товара из корзины:'}
                              closeButton={true}
                              text={`Вы хотите удалить ${title} из корзины?`}
                              isOpen={modalIsOpen}
                              closeFunc={cancelButtonFunction}>
                <Button text={'OK'}
                        onClick={removingFromCart}
                        className={'button'}
                        backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                <Button text={'Cancel'}
                        onClick={cancelButtonFunction}
                        className={'button'}
                        backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
            </Modal>}
        </>
    );


    function favoriteClick() {
        toFavorites(id)
    }

    function openModal() {
        setOpenModal(true);
    }

    function addingToCart() {
        addToCart(id);
        setOpenModal(false)
    }

    function removingFromCart() {
        removeFromCart(id);
        setOpenModal(false)
    }

    function cancelButtonFunction() {
        setOpenModal(false)
    }
}

Modal.propTypes = {
    item: PropTypes.array,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
}

Modal.defaultProps = {
    item: [],
    inFavorites: false,
    inCart: false,
};

export default Card;