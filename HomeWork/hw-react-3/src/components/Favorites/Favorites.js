import React from 'react';
import PropTypes from "prop-types";

function Favorites({favorites}) {
    return (
        <div>
            В Избранном: {favorites.length}
        </div>
    );
}

Favorites.propTypes = {
    items: PropTypes.array,
};

Favorites.defaultProps = {
    items: [],
};

export default Favorites;