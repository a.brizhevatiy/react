import React from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import './Body.scss'

function Body({items, toFavorites, addToCart, removeFromCart, cart, favorites}) {
    return (
        <div className='body'>
            <Cards items={items}
                   toFavorites={toFavorites}
                   addToCart={addToCart}
                   removeFromCart={removeFromCart}
                   cart={cart}
                   favorites={favorites}/>
        </div>
    );
}

Body.propTypes = {
    items: PropTypes.array,
    toFavorites: PropTypes.func,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    cart: PropTypes.array,
    favorites: PropTypes.array,
};

Body.defaultProps = {
    items: [],
    toFavorites: () => {
    },
    addToCart: () => {
    },
    removeFromCart: () => {
    },
    cart: [],
    favorites: [],
};

export default Body;