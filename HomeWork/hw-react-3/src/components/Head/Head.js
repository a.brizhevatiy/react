import React from 'react';
import Navigation from "../Navigation/Navigation";
import './Head.scss'

function Head() {
    return (
        <div className={'head'}>
            <Navigation/>
        </div>
    );
}

export default Head;