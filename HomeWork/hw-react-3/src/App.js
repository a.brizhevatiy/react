import React, {useState, useEffect} from 'react';
import axios from "axios";
import Loader from "./components/Loader/Loader";
import Head from './components/Head/Head'
import Routes from "./Routes/Routes";
import "./App.css"

function App() {
    const [items, setItems] = useState({items: null, loaded: false})

    useEffect(() => {
        setTimeout(() => {
            axios('data/items.json')
                .then(res => setItems({items: res.data, loaded: true})
                );
        }, 1000)
    }, [])

    const [cart, setCart] = useState([])
    const [favorites, setFavorites] = useState([])

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem('cart'));
        const favorites = JSON.parse(localStorage.getItem('favorites'));
        if (cart) {
            setCart(cart);
        }
        if (favorites) {
            setFavorites(favorites);
        }
    }, [items.loaded])

    useEffect(() => {
        localStorage.setItem('cart', JSON.stringify(cart))
    }, [cart]);

    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites]);

    return (
        <div className='App'>
            <Head/>
            {!items.loaded && <Loader/>}
            {items.loaded && <Routes items={items.items}
                                     toFavorites={toFavorites}
                                     addToCart={addToCart}
                                     removeFromCart={removeFromCart}
                                     cart={cart}
                                     favorites={favorites}
            />}
        </div>
    );

    function toFavorites(id) {
        let newArr = [];
        if (favorites.indexOf(id) !== -1) {
            newArr = favorites.filter(c => c !== id);
        } else {
            newArr = [...favorites, id];
        }
        setFavorites(newArr);
    }

    function addToCart(id) {
        const newArr = [...cart, id];
        setCart(newArr);
    }

    function removeFromCart(id) {
        const newArr = cart.filter(c => c !== id);
        setCart(newArr);
    }
}

export default App;