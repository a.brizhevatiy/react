import React from 'react';
import {Route, Switch} from 'react-router-dom';
import FavoritesPage from "../components/FavoritesPage/FavoritesPage";
import CartPage from "../components/CartPage/CartPage";
import Body from "../components/Body/Body";
import p404 from "../components/p404/p404";
import FloatWindow from "../components/FloatWindow/FloatWindow";
import PropTypes from "prop-types";

function Routes({items, toFavorites, addToCart, removeFromCart, cart, favorites}) {
    return (
        <>
            <Switch>
                <Route exact path='/' render={() => <Body items={items}
                                                          toFavorites={toFavorites}
                                                          addToCart={addToCart}
                                                          removeFromCart={removeFromCart}
                                                          cart={cart}
                                                          favorites={favorites}
                />}/>
                <Route exact path='/favorites' render={() => <FavoritesPage items={items}
                                                                            toFavorites={toFavorites}
                                                                            addToCart={addToCart}
                                                                            removeFromCart={removeFromCart}
                                                                            cart={cart}
                                                                            favorites={favorites}
                />}/>
                <Route exact path='/cart' render={() => <CartPage items={items}
                                                                  toFavorites={toFavorites}
                                                                  addToCart={addToCart}
                                                                  removeFromCart={removeFromCart}
                                                                  cart={cart}
                                                                  favorites={favorites}
                />}/>
                <Route path='*' component={p404}/>
            </Switch>
            <FloatWindow cart={cart}
                         favorites={favorites}/>
        </>
    );
}

Routes.propTypes = {
    items: PropTypes.array,
    toFavorites: PropTypes.func,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    cart: PropTypes.array,
    favorites: PropTypes.array,
}
Routes.defaultProps = {
    items: [],
    toFavorites: () => {
    },
    addToCart: () => {
    },
    removeFromCart: () => {
    },
    cart: [],
    favorites: [],
}

export default Routes;