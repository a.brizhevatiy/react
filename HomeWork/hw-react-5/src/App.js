import React, { useEffect } from "react";
import Loader from "./components/Loader/Loader";
import Head from "./components/Head/Head";
import Routes from "./Routes/Routes";
import FloatWindow from "./components/FloatWindow/FloatWindow";
import PropTypes from "prop-types";
import { connect, useDispatch } from "react-redux";
import { changeCart, changeFavorites, loadItems } from "./store/App/actions";
import "./App.css";
import ModalWrapper from "./components/Modal/ModalWrapper";

function App({
    itemsList,
    loaded,
    cart,
    favorites,
    changeCart,
    changeFavorites,

}) {
    const dispatch = useDispatch();
    useEffect(() => {
        if (!itemsList.length) {
            dispatch(loadItems());
        }
    }, []);

    useEffect(() => {
        const cart = JSON.parse(localStorage.getItem("cart"));
        const favorites = JSON.parse(localStorage.getItem("favorites"));
        if (cart) {
            changeCart(cart);
        }
        if (favorites) {
            changeFavorites(favorites);
        }
    }, [loaded]);

    useEffect(() => {
        localStorage.setItem("cart", JSON.stringify(cart));
    }, [cart]);

    useEffect(() => {
        localStorage.setItem("favorites", JSON.stringify(favorites));
    }, [favorites]);

    return (
        <div className="App">
            <Head />
            {!loaded && <Loader />}
            {loaded && <Routes />}
            <ModalWrapper/>
            <FloatWindow />
        </div>
    );
}

App.propTypes = {
    cart: PropTypes.array,
    favorites: PropTypes.array,
    changeFavorites: PropTypes.func,
    itemsList: PropTypes.array,
    loaded: PropTypes.bool,
    changeCart: PropTypes.func,
};

App.defaultProps = {
    itemsList: [],
    loaded: false,
    cart: [],
    favorites: [],
    id: null,
    title: "",
    isInCart: false,
    changeCart: () => {},
    changeFavorites: () => {},
};

const mapStateToProps = (state) => {
    return {
        itemsList: state.app.items.itemsList,
        loaded: state.app.items.loaded,
        cart: state.app.cart,
        favorites: state.app.favorites,
        id: state.modal.id,
        title: state.modal.title,
        isInCart: state.modal.inCart,
    };
};

const mapDispatchToProps = {
    changeCart,
    changeFavorites,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
