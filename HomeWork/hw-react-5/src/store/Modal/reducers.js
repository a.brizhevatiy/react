import {
    IS_MODAL_OPEN,
    SET_MODAL_TITLE,
    SET_MODAL_ID,
    IS_IN_CART,
} from "./actions";

const defaultState = {
    isOpen: false,
    title: "",
    id: null,
    inCart: false,
};

export const modalReducer = (state = defaultState, action) => {
    switch (action.type) {
        case IS_MODAL_OPEN:
            return { ...state, isOpen: action.payload };
        case SET_MODAL_TITLE:
            return { ...state, title: action.payload };
        case SET_MODAL_ID:
            return { ...state, id: action.payload };
        case IS_IN_CART:
            return { ...state, inCart: action.payload };
        default:
            return state;
    }
};
