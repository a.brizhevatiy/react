import React from "react";
import { useField, Field } from "formik";
import "./CartFormFormik.scss";

export default function FormField({ label, name, ...rest }) {
    const [field, meta, helpers] = useField(name);

    return (
        <>
            <div>
                <label>
                    {label}
                    <br />
                    <Field {...field} {...rest} />
                </label>
            </div>
            {meta.touched && meta.error && (
                <div className="field-error">{meta.error}</div>
            )}
        </>
    );
}
