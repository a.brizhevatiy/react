import React from "react";
import PropTypes from "prop-types";
import Button from "../Button/Button";
import "./Modal.scss";
import { connect } from "react-redux";
import { setOpenModal } from "../../store/Modal/actions";

function Modal({
    header,
    closeButton,
    text,
    children,
    modalIsOpen,
    setOpenModal,
}) {
    return (
        <>
            {modalIsOpen && (
                <div className="modalOverlay" onClick={overlayClose}>
                    <div className="modal">
                        <div className="modalHeader">
                            <div className="modalTitle">{header}</div>
                            {closeButton && (
                                <Button
                                    className="modalCloseButton"
                                    text={"X"}
                                    onClick={() => setOpenModal(false)}
                                />
                            )}
                        </div>
                        <div className="modalBody">{text}</div>
                        <div className="modalFooter">{children}</div>
                    </div>
                </div>
            )}
        </>
    );

    function overlayClose(e) {
        e.persist();
        if (e.target.classList.contains("modalOverlay")) {
            setOpenModal(false);
        }
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    children: PropTypes.node,
    modalIsOpen: PropTypes.bool,
    setOpenModal: PropTypes.func,
};

Modal.defaultProps = {
    header: "",
    closeButton: false,
    text: "",
    children: null,
    modalIsOpen: false,
    setOpenModal: () => {},
};

const mapStateToProps = (state) => {
    return {
        modalIsOpen: state.modal.isOpen,
    };
};

const mapDispatchToProps = dispatch =>{
    return{
        setOpenModal: ()=>dispatch(setOpenModal()),

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
