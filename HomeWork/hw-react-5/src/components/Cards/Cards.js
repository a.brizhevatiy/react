import React from "react";
import Card from "../Card/Card";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "./Cards.scss";
import { changeFavorites } from "../../store/App/actions";

function Cards({ items, changeFavorites, cart, favorites }) {
    const itemCards = items.map((i) => (
        <Card
            key={i.id}
            item={i}
            id={i.id}
            toFavorites={toFavorites}
            cart={cart}
            favorites={favorites}
            inCart={isInArr(i.id, cart)}
            inFavorites={isInArr(i.id, favorites)}
        />
    ));

    return <>{itemCards}</>;

    function toFavorites(id) {
        let newArr = [];
        if (favorites.indexOf(id) !== -1) {
            newArr = favorites.filter((c) => c !== id);
        } else {
            newArr = [...favorites, id];
        }
        changeFavorites(newArr);
    }
}

function isInArr(id, arr) {
    return arr.indexOf(id) !== -1;
}

Cards.propTypes = {
    cart: PropTypes.array,
    items: PropTypes.array,
    favorites: PropTypes.array,
    changeFavorites: PropTypes.func,
};

Cards.defaultProps = {
    cart: [],
    items: [],
    favorites: [],
    changeFavorites: () => {},
};

const mapStateToProps = (state) => {
    return {
        cart: state.app.cart,
        favorites: state.app.favorites,
    };
};

const mapDispatchToProps = {
    changeFavorites,
};

export default connect(mapStateToProps, mapDispatchToProps)(Cards);
