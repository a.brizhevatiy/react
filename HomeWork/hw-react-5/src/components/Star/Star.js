import React from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faStar} from "@fortawesome/free-regular-svg-icons"
import {faStar as solidStar} from '@fortawesome/free-solid-svg-icons'
import PropTypes from "prop-types";
import "./Star.scss"

function Star({inFavorites, onClick}) {
    const star = inFavorites ? <FontAwesomeIcon icon={solidStar}/> : <FontAwesomeIcon icon={faStar}/>;
    return <span className='star' onClick={onClick}>{star}</span>;
}

Star.propTypes = {
    inFavorites: PropTypes.bool,
    onClick: PropTypes.func,
};

Star.defaultProps = {
    inFavorites: false,
    onClick: () => {
    },
};

export default Star;