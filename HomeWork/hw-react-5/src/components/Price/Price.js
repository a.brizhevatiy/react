import React from 'react';
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";
import "./Price.scss"

const Price = ({price}) => <span className='price'>Цена: {price}грн</span>;

Modal.propTypes = {
    price: PropTypes.number,
};

Modal.defaultProps = {
    price: 0,
};

export default Price;