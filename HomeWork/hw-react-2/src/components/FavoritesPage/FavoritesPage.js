import React, {Component} from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import "./FavoritesPage.scss"

class FavoritesPage extends Component {
    render() {
        const {items, cart, toFavorites, addToCart, removeFromCart, favorites} = this.props;
        return (
            <>
                {favorites.length === 0 &&
                <div className={'fav__empty'}>В Избранном ничего нет...</div>}

                {favorites.length !== 0 &&
                <div className={'fav'}>
                    <Cards items={items.filter(item => favorites.indexOf(item.id) !== -1)}
                           toFavorites={toFavorites}
                           addToCart={addToCart}
                           removeFromCart={removeFromCart}
                           favorites={favorites}
                           cart={cart}/>
                </div>}
            </>
        );
    }
}

FavoritesPage.propTypes = {
    items: PropTypes.array,
    cart: PropTypes.array,
    toFavorites: PropTypes.func,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    favorites: PropTypes.array,
};

FavoritesPage.defaultProps = {
    items: [],
    cart: [],
    toFavorites: () => {
    },
    addToCart: () => {
    },
    removeFromCart: () => {
    },
    favorites: [],
};


export default FavoritesPage;