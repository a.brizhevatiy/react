import React, {PureComponent} from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import './Body.scss'

class Body extends PureComponent {

    render() {
        const {items, toFavorites, addToCart, removeFromCart, cart, favorites} = this.props;

        return (
            <div className='body'>
                <Cards items={items}
                       toFavorites={toFavorites}
                       addToCart={addToCart}
                       removeFromCart={removeFromCart}
                       cart={cart}
                       favorites={favorites}/>
            </div>
        );
    }
}

Body.propTypes = {
    items: PropTypes.array,
    toFavorites: PropTypes.func,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    cart: PropTypes.array,
    favorites: PropTypes.array,
};

Body.defaultProps = {
    items: [],
    toFavorites: ()=>{},
    addToCart: ()=>{},
    removeFromCart: ()=>{},
    cart: [],
    favorites: [],
};

export default Body;