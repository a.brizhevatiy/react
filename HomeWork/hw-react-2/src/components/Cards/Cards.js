import React, {PureComponent} from 'react';
import Card from "../Card/Card";
import PropTypes from "prop-types";
import "./Cards.scss"

class Cards extends PureComponent {
    render() {
        const {items,  addToCart, removeFromCart, toFavorites, cart, favorites} = this.props;
        const itemCards = items.map(i => <Card key={i.id}
                                               item={i}
                                               id={i.id}
                                               toFavorites={toFavorites}
                                               addToCart={addToCart}
                                               removeFromCart={removeFromCart}
                                               cart={cart}
                                               favorites={favorites}
                                               inCart={this.isInArr(i.id, cart)}
                                               inFavorites={this.isInArr(i.id, favorites)}/>);

        return (
            <>
                {itemCards}
            </>
        );
    }

    isInArr = (id, arr) => {
        return arr.indexOf(id) !== -1;
    }
}

Cards.propTypes = {
    items: PropTypes.array,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    toFavorites: PropTypes.func,
    cart: PropTypes.array,
    favorites: PropTypes.array,
};

Cards.defaultProps = {
    items: [],
    addToCart: ()=>{},
    removeFromCart: ()=>{},
    toFavorites: ()=>{},
    cart: [],
    favorites: [],
};

export default Cards;