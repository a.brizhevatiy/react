import React, {Component} from 'react';
import './p404.scss'

class p404
    extends Component {
    render() {
        return (
            <div className={'p404'}>
                Page not found!
            </div>
        );
    }
}

export default p404;