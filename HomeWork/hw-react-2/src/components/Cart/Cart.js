import React, {PureComponent} from 'react';
import PropTypes from "prop-types";

class Cart extends PureComponent {
    render() {
        const {cart} = this.props
        return (
            <div>
                В Корзине: {cart.length}
            </div>
        );
    }
}

Cart.propTypes = {
    cart: PropTypes.array,
};

Cart.defaultProps = {
    cart: [],
};

export default Cart;