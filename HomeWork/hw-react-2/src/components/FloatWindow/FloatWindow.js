import React, {PureComponent} from 'react';
import Cart from "../Cart/Cart";
import Favorites from "../Favorites/Favorites";
import PropTypes from "prop-types";
import './FloatWindow.scss'

class FloatWindow extends PureComponent {
    render() {
        const {cart, favorites} = this.props;
        return (
            <div className='window'>
                <Favorites favorites={favorites}/>
                <Cart cart={cart}/>
            </div>
        );
    }
}

Favorites.propTypes = {
    cart: PropTypes.array,
    favorites: PropTypes.array,
};

Favorites.defaultProps = {
    cart: [],
    favorites: [],
};

export default FloatWindow;