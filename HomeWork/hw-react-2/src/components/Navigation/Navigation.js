import React, {PureComponent} from 'react';
import { NavLink} from 'react-router-dom';
import './Navigation.scss';

class Navigation extends PureComponent {
    render() {

        return <nav>
            <ul className={'nav'}>
                <li><NavLink exact to='/' className='nav__link' activeClassName='nav__link-active'>Главная</NavLink></li>
                <li><NavLink to='/favorites' className='nav__link' activeClassName='nav__link-active'>Избранное</NavLink></li>
                <li><NavLink to='/cart' className='nav__link' activeClassName='nav__link-active'>Корзина</NavLink></li>
            </ul>
        </nav>
    }
}

export default Navigation;