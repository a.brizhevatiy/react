import React, {PureComponent} from 'react';
import Navigation from "../Navigation/Navigation";
import './Head.scss'

class Head extends PureComponent {

    render() {
        return (
            <div className={'head'}>
                <Navigation />
            </div>
        );
    }
}

export default Head;