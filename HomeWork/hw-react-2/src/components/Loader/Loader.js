import React, { PureComponent } from 'react'

class Loader extends PureComponent {
    render() {
        return <h2>Ждемс...</h2>
    }
}

export default Loader;
