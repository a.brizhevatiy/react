import React, {PureComponent} from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import "./CartPage.scss"

class CartPage extends PureComponent {
    render() {
        const {items, cart, toFavorites, addToCart, removeFromCart, favorites} = this.props;
        return (
            <>
                {cart.length === 0 &&
                <div className={'cart__empty'}>В Корзину ничего не добавлено... :`(</div>}

                {cart.length !== 0 &&
                <div className={'cart'}>
                    <Cards items={items.filter(item => cart.indexOf(item.id) !== -1)}
                           toFavorites={toFavorites}
                           addToCart={addToCart}
                           removeFromCart={removeFromCart}
                           favorites={favorites}
                           cart={cart}/>
                </div>}
            </>
        );
    }
}

CartPage.propTypes = {
    items: PropTypes.array,
    cart: PropTypes.array,
    toFavorites: PropTypes.func,
    addToCart: PropTypes.func,
    removeFromCart: PropTypes.func,
    favorites: PropTypes.array,
};

CartPage.defaultProps = {
    items: [],
    cart: [],
    toFavorites: () => {
    },
    addToCart: () => {
    },
    removeFromCart: () => {
    },
    favorites: [],
};

export default CartPage;