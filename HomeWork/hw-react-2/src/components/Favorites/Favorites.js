import React, {PureComponent} from 'react';
import PropTypes from "prop-types";

class Favorites extends PureComponent {
    render() {
        const {favorites} = this.props
        return (
            <div>
                В Избранном: {favorites.length}
            </div>
        );
    }
}

Favorites.propTypes = {
    items: PropTypes.array,
};

Favorites.defaultProps = {
    items: [],
};

export default Favorites;