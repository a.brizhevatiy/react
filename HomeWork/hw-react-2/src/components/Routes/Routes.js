import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import FavoritesPage from "../FavoritesPage/FavoritesPage";
import CartPage from "../CartPage/CartPage";
import Body from "../Body/Body";
import p404 from "../p404/p404";
import FloatWindow from "../FloatWindow/FloatWindow";
import PropTypes from "prop-types";

class Routes extends Component {
    state = {
        cart: [],
        favorites: [],
    }

    componentDidMount() {
        const state = JSON.parse(localStorage.getItem('state'));
        if (state) {
            this.setState(state);
        }
    }

    componentDidUpdate() {
        localStorage.setItem('state', JSON.stringify(this.state))
    }

    render() {
        const {cart, favorites} = this.state;
        const {items} = this.props;
        return (<>
                <Switch>
                    <Route exact path='/' render={(routerProps) => <Body items={items}
                                                                         toFavorites={this.toFavorites}
                                                                         addToCart={this.addToCart}
                                                                         removeFromCart={this.removeFromCart}
                                                                         cart={cart}
                                                                         favorites={favorites}
                                                                         {...routerProps}/>}/>
                    <Route exact path='/favorites' render={(routerProps) => <FavoritesPage items={items}
                                                                                           toFavorites={this.toFavorites}
                                                                                           addToCart={this.addToCart}
                                                                                           removeFromCart={this.removeFromCart}
                                                                                           cart={cart}
                                                                                           favorites={favorites}
                                                                                           {...routerProps}/>}/>
                    <Route exact path='/cart' render={(routerProps) => <CartPage items={items}
                                                                                 toFavorites={this.toFavorites}
                                                                                 addToCart={this.addToCart}
                                                                                 removeFromCart={this.removeFromCart}
                                                                                 cart={cart}
                                                                                 favorites={favorites}
                                                                                 {...routerProps}/>}/>
                    <Route path='*' component={p404}/>
                </Switch>
                <FloatWindow cart={cart}
                             favorites={favorites}/>
            </>
        );
    }

    toFavorites = (id) => {
        let newArr = [];
        if (this.state.favorites.indexOf(id) !== -1) {
            newArr = this.state.favorites.filter(c => c !== id);
        } else {
            newArr = [...this.state.favorites, id];
        }
        this.setState({favorites: newArr});
    }

    addToCart = (id) => {
        const newArr = [...this.state.cart, id];
        this.setState({cart: newArr})
    }

    removeFromCart = (id) => {
        const newArr = this.state.cart.filter(c => c !== id);
        this.setState({cart: newArr});
    }
}

Routes.propTypes = {
    items: PropTypes.array
}
Routes.defaultProps = {
    item: []
}

export default Routes;