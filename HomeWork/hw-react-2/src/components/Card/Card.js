import React, {PureComponent} from 'react';
import Image from "../Image/Image";
import Star from "../Star/Star";
import Button from "../Button/Button";
import Modal from "../Modal/Modal";
import Price from "../Price/Price";
import PropTypes from "prop-types";
import "./Card.scss"

class Card extends PureComponent {
    state = {
        openModal: false
    }

    render() {
        const {item, inCart, inFavorites} = this.props;
        const {openModal} = this.state;
        const {title, price, img,} = item;
        const buttonText = !inCart ? 'В корзину' : 'Из корзины';
        const backgroundColor = buttonText === 'Из корзины' ? 'rgba(255, 0, 0, 0.1)' : 'rgba(0, 0, 0, 0.2)';
        return (
            <>
                <div className='card'>
                    <h3 className="card__title">{title} <Star inFavorites={inFavorites}
                                                              onClick={this.favoriteClick}/>
                    </h3>
                    <div className="card__image">
                        <Image src={img}/>
                    </div>
                    <div className='card__price'><Price price={price}/></div>

                    {/*!--------------------------!*/}
{/*<div>Color: {color}</div> а хз к чему его но в тз просили ))))))*/}
                    {/*!--------------------------!*/}

                    <div>
                        <Button text={buttonText}
                                onClick={this.openModal}
                                className={'button'}
                                backgroundColor={backgroundColor}/>
                    </div>
                </div>
                {!inCart && <Modal header={'Добавление товара в корзину:'}
                                   closeButton={true}
                                   text={`Вы хотите добавить ${title} в корзину?`}
                                   isOpen={openModal}
                                   closeFunc={this.cancelButtonFunction}>
                    <Button text={'OK'}
                            onClick={this.addToCart}
                            className={'button'}
                            backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                    <Button text={'Cancel'}
                            onClick={this.cancelButtonFunction}
                            className={'button'}
                            backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                </Modal>}
                {inCart && <Modal header={'Удаление товара из корзины:'}
                                  closeButton={true}
                                  text={`Вы хотите удалить ${title} из корзины?`}
                                  isOpen={openModal}
                                  closeFunc={this.cancelButtonFunction}>
                    <Button text={'OK'}
                            onClick={this.removeFromCart}
                            className={'button'}
                            backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                    <Button text={'Cancel'}
                            onClick={this.cancelButtonFunction}
                            className={'button'}
                            backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                </Modal>}
            </>
        );
    }

    favoriteClick = () => {
        this.props.toFavorites(this.props.id)
    }

    openModal = () => {
        this.setState({openModal: true})
    }

    addToCart = () => {
        const {addToCart, id} = this.props;
        addToCart(id);
        this.setState({openModal: false});
    }

    removeFromCart = () => {
        const {removeFromCart, id} = this.props;
        removeFromCart(id);
        this.setState({openModal: false})
    }

    cancelButtonFunction = () => {
        this.setState({openModal: false});
    }
}

Modal.propTypes = {
    item: PropTypes.array,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
}

Modal.defaultProps = {
    item: [],
    inFavorites: false,
    inCart: false,
};

export default Card;