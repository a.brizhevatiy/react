import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Modal from "../Modal/Modal";
import "./Price.scss"

class Price extends PureComponent {
    render() {
        const {price} = this.props;

        return (
            <span className='price'>
                Цена: {price}грн
            </span>
        );
    }
}

Modal.propTypes = {
    price: PropTypes.number,
};

Modal.defaultProps = {
    price: 0,
};

export default Price;