import React, {Component} from 'react';
import axios from "axios";
import Loader from "./components/Loader/Loader";
import "./App.css"
import Head from './components/Head/Head'
import Routes from "./components/Routes/Routes";



class App extends Component {
    state = {
        items: [],
        isLoading: true
    }

    componentDidMount() {
        setTimeout(() => {
            axios('data/items.json')
                .then(res => this.setState({items: res.data, isLoading: false}))
        }, 1000)
    }

    render() {
        const {items, isLoading} = this.state;
        return (
            <div className='App'>
                <Head />
                {isLoading && <Loader/>}
                {!isLoading && <Routes items={items}/>}
            </div>
        );
    }
}

export default App;