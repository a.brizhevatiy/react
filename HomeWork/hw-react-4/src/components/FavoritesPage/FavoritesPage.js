import React from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import "./FavoritesPage.scss"
import {connect} from 'react-redux';

function FavoritesPage({items, favorites}) {
    return (
        <>
            {!favorites.length &&
            <div className={'fav__empty'}>В Избранном ничего нет...</div>}

            {!!favorites.length &&
            <div className={'fav'}>
                <Cards
                    items={items.filter(item => favorites.indexOf(item.id) !== -1)}
                />
            </div>}
        </>
    );
}

FavoritesPage.propTypes = {
    items: PropTypes.array,
    favorites: PropTypes.array,
};

FavoritesPage.defaultProps = {
    items: [],
    favorites: [],
};
const mapStateToProps = (state) => {
    return {
        items: state.app.items.itemsList,
        favorites: state.app.favorites,
    }
}
export default connect(mapStateToProps)(FavoritesPage);