import React from "react";
import Image from "../Image/Image";
import Star from "../Star/Star";
import Button from "../Button/Button";
import Price from "../Price/Price";
import PropTypes from "prop-types";
import "./Card.scss";
import { connect } from "react-redux";
import {
    setOpenModal,
    modalTitle,
    modalId,
    isInCart,
} from "../../store/Modal/actions";

function Card({
    item,
    inCart,
    inFavorites,
    id,
    toFavorites,
    setOpenModal,
    modalTitle,
    modalId,
    isInCart,
}) {
    const { title, price, img } = item;
    const buttonText = !inCart ? "В корзину" : "Из корзины";
    const backgroundColor =
        buttonText === "Из корзины"
            ? "rgba(255, 0, 0, 0.1)"
            : "rgba(0, 0, 0, 0.2)";
    return (
        <>
            <div className="card">
                <h3 className="card__title">
                    {title}
                    <Star inFavorites={inFavorites} onClick={favoriteClick} />
                </h3>
                <div className="card__image">
                    <Image src={img} />
                </div>
                <div className="card__price">
                    <Price price={price} />
                </div>

                {/*!--------------------------!*/}
                {/*<div>Color: {color}</div> а хз к чему его но в тз просили ))))))*/}
                {/*!--------------------------!*/}

                <div>
                    <Button
                        text={buttonText}
                        onClick={openModal}
                        className={"button"}
                        backgroundColor={backgroundColor}
                    />
                </div>
            </div>
        </>
    );

    function favoriteClick() {
        toFavorites(id);
    }

    function openModal() {
        setOpenModal(true);
        modalId(id);
        modalTitle(title);
        isInCart(inCart);
    }
}

Card.propTypes = {
    item: PropTypes.object,
    id: PropTypes.number,
    inCart: PropTypes.bool,
    inFavorites: PropTypes.bool,
    modalIsOpen: PropTypes.bool,
    toFavorites: PropTypes.func,
    setOpenModal: PropTypes.func,
    modalTitle: PropTypes.func,
    modalId: PropTypes.func,
    isInCart: PropTypes.func,
};

Card.defaultProps = {
    id: null,
    item: {},
    inCart: false,
    inFavorites: false,
    modalIsOpen: false,
    toFavorites: () => {},
    setOpenModal: () => {},
    modalTitle: () => {},
    modalId: () => {},
    isInCart: () => {},
};

const mapStateToProps = (state) => {
    return {
        modalIsOpen: state.modal.isOpen,
    };
};

const mapDispatchToProps = {
    setOpenModal,
    modalTitle,
    modalId,
    isInCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(Card);
