import React from "react";
import Cart from "../Cart/Cart";
import Favorites from "../Favorites/Favorites";
import "./FloatWindow.scss";

function FloatWindow() {
    return (
        <div className="window">
            <Favorites />
            <Cart />
        </div>
    );
}

export default FloatWindow;
