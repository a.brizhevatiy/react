import React from 'react';
import './page404.scss'

const p404 = () => <div className={'p404'}>Page not found!</div>;

export default p404;