import React from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import "./CartPage.scss"
import {connect} from 'react-redux';

function CartPage({items, cart}) {
    return (
        <>
            {!cart.length&&
            <div className={'cart__empty'}>В Корзину ничего не добавлено... :`(</div>}

            {!!cart.length &&
            <div className={'cart'}>
                <Cards items={items.filter(item => cart.indexOf(item.id) !== -1)}
                />
            </div>}
        </>
    );
}

CartPage.propTypes = {
    items: PropTypes.array,
    cart: PropTypes.array,
};

CartPage.defaultProps = {
    items: [],
    cart: [],
};
const mapStateToProps = (state) => {
    return {
        items: state.app.items.itemsList,
        cart: state.app.cart,
    }
}
export default connect(mapStateToProps)(CartPage);