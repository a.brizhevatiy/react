import React from "react";
import PropTypes from "prop-types";
import "./Image.scss";

function Image({ src, alt, width, height, ...attrs }) {
    return (
        <img
            className="image"
            src={src}
            alt={alt}
            width={width}
            height={height}
            {...attrs}
        />
    );
}

Image.propTypes = {
    src: PropTypes.string,
    alt: PropTypes.string,
    width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    attrs: PropTypes.any,
};

Image.defaultProps = {
    src: "",
    alt: "image",
    width: "100%",
    height: "",
    attrs: "",
};

export default Image;
