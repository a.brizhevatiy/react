import React from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

function Cart({cart}) {
    return (
        <div>
            В Корзине: {cart.length}
        </div>
    );
}

Cart.propTypes = {
    cart: PropTypes.array,
};

Cart.defaultProps = {
    cart: [],
};
const mapStateToProps = (state) => {
    return {
        cart: state.app.cart,
    };
};

export default connect(mapStateToProps)(Cart);