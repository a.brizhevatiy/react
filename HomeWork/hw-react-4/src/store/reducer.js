import {combineReducers} from "redux";
import {appReducer} from './App/reducers';
import {modalReducer} from "./Modal/reducers";

export default combineReducers({
    app: appReducer,
    modal: modalReducer,
})