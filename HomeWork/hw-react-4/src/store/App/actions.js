import axios from "axios";

export const SAVE_ITEMS = "SAVE_ITEMS";
export const ITEMS_LOADED = "ITEMS_LOADED";
export const CHANGE_CART = "CHANGE_CART";
export const CHANGE_FAVORITES = "CHANGE_FAVORITES";

export const saveItems = (items) => (dispatch) => {
    dispatch({
        type: SAVE_ITEMS,
        payload: items,
    });
};
export const itemsLoaded = (loaded) => (dispatch) => {
    dispatch({
        type: ITEMS_LOADED,
        payload: loaded,
    });
};
export const changeCart = (newCart) => (dispatch) => {
    dispatch({
        type: CHANGE_CART,
        payload: newCart,
    });
};
export const changeFavorites = (newFav) => (dispatch) => {
    dispatch({
        type: CHANGE_FAVORITES,
        payload: newFav,
    });
};

export const loadItems = () => (dispatch) => {
    itemsLoaded(false);
    axios("data/items.json").then((res) => {
        dispatch(saveItems(res.data));
        dispatch(itemsLoaded(true));
    });
};
