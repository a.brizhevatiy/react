import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import './Modal.scss'
import Button from "../Button/Button";

class Modal extends PureComponent {
    render() {
        const {header, closeButton, text, action, closeFunc} = this.props;

        return (
            <>
                <div className='modalOverlay' onClick={this.overlayClose}>
                    <div className='modal'>
                        <div className="modalHeader">
                            <div className="modalTitle">
                                {header}
                            </div>
                            {closeButton &&
                            <Button className='modalCloseButton' text={'X'} onClick={closeFunc}/>
                            }
                        </div>
                        <div className="modalBody">
                            {text}
                        </div>
                        <div className="modalFooter">
                            {[...action]}
                        </div>
                    </div>
                </div>
            </>
        );
    }

    overlayClose = (e) => {
        e.persist();
        if (e.target.classList.contains('modalOverlay')) {
            this.props.closeFunc();
        }
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    children: PropTypes.node,
    isOpen: PropTypes.bool,
    closeFunc: PropTypes.func,
};

Modal.defaultProps = {
    header: '',
    closeButton: false,
    text: '',
    children: null,
    isOpen: false,
    closeFunc: () => {
    },
};

export default Modal;