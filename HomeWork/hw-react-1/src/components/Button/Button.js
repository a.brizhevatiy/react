import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

class Button extends PureComponent {
    render() {
        const {text, onClick, backgroundColor, className} = this.props;
        const style = {
            backgroundColor: backgroundColor,
        }

        return (
            <button className={className} style={style} onClick={onClick}>{text}</button>
        );
    }
}

Button.propTypes = {
    text: PropTypes.string,
    onClick: PropTypes.func,
    className: PropTypes.string,
    backgroundColor: PropTypes.string,
};

Button.defaultProps = {
    text: '',
    onClick: () => {},
    className: '',
    backgroundColor: '',
};

export default Button;