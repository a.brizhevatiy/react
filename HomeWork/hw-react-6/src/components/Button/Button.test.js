import React from "react";
import Button from "./Button";
import { fireEvent, render } from "@testing-library/react";

describe("Button testing", () => {
    test("Smoke testing", () => {
        render(<Button />);
    });

    test("Show prop 'text' in button", () => {
        const btnText = "Some cool text";
        render(<Button text={btnText} m />);
        const btn = document.getElementsByTagName("button")[0];
        expect(btn.textContent).toBe(btnText);
    });

    test("Button click check", () => {
        const onClickFunc = jest.fn();
        render(<Button onClick={onClickFunc} />);
        const btn = document.getElementsByTagName("button")[0];
        expect(onClickFunc).not.toHaveBeenCalled();
        fireEvent.click(btn);
        expect(onClickFunc).toHaveBeenCalled();
        expect(onClickFunc).toHaveBeenCalledTimes(1);
    });
});
