import React from "react";
import ModalWrapper from "./ModalWrapper";
import { render } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
let testStore;
jest.mock("./Modal", () => (props) => <div>Modal!</div>);

beforeEach(() => {
    testStore = mockStore({
        app: {
            items: { itemsList: [], loaded: false },
            cart: [],
            favorites: [],
            isOpen: false,
        },
        modal: { isOpen: false, title: "", id: null, inCart: false },
        sform: { formData: {}, formIsShow: false, userIsReady: false },
    });
});

describe("Modal wrapper testing", () => {
    test("Smoke testing", () => {
        render(
            <Provider store={testStore}>
                <ModalWrapper />
            </Provider>
        );
    });

    test("Modal render from here", () => {
        const { getByText } = render(
            <Provider store={testStore}>
                <ModalWrapper />
            </Provider>
        );
        const modal = getByText("Modal!");
        expect(modal).toBeInTheDocument();
    });
});
