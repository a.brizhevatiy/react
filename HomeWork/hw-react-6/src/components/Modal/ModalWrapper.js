import Modal from "./Modal";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import React from "react";
import { changeCart } from "../../store/App/actions";
import { setOpenModal, modalTitle, modalId } from "../../store/Modal/actions";

export function ModalWrapper({ cart, changeCart, setOpenModal, id, title, isInCart }) {
    return (
        <>
            {!isInCart && (
                <Modal
                    header={"Добавление товара в корзину:"}
                    closeButton={true}
                    text={`Вы хотите добавить ${title} в корзину?`}
                >
                    <Button
                        text={"OK"}
                        onClick={addToCart}
                        className={"button"}
                        backgroundColor={"rgba(0, 0, 0, 0.2)"}
                    />
                    <Button
                        text={"Cancel"}
                        onClick={() => setOpenModal(false)}
                        className={"button"}
                        backgroundColor={"rgba(0, 0, 0, 0.2)"}
                    />
                </Modal>
            )}
            {isInCart && (
                <Modal
                    header={"Удаление товара из корзины:"}
                    closeButton={true}
                    text={`Вы хотите удалить ${title} из корзины?`}
                >
                    <Button
                        text={"OK"}
                        onClick={removeFromCart}
                        className={"button"}
                        backgroundColor={"rgba(0, 0, 0, 0.2)"}
                    />
                    <Button
                        text={"Cancel"}
                        onClick={() => setOpenModal(false)}
                        className={"button"}
                        backgroundColor={"rgba(0, 0, 0, 0.2)"}
                    />
                </Modal>
            )}
        </>
    );

    function addToCart() {
        const newArr = [...cart, id];
        changeCart(newArr);
        setOpenModal(false);
    }

    function removeFromCart() {
        const newArr = cart.filter((c) => c !== id);
        changeCart(newArr);
        setOpenModal(false);
    }
}

ModalWrapper.propTypes = {
    cart: PropTypes.array,
    changeCart: PropTypes.func,
    setOpenModal: PropTypes.func,
    id: PropTypes.number,
    title: PropTypes.string,
    isInCart: PropTypes.bool,
};

ModalWrapper.defaultProps = {
    cart: [],
    changeCart: () => {},
    setOpenModal: () => {},
    modalTitle: () => {},
    modalId: () => {},
};

const mapStateToProps = (state) => {
    return {
        cart: state.app.cart,
        id: state.modal.id,
        title: state.modal.title,
        isInCart: state.modal.inCart,
    };
};

const mapDispatchToProps = {
    changeCart,
    setOpenModal,
    modalTitle,
    modalId,
};

export default connect(mapStateToProps, mapDispatchToProps)(ModalWrapper);
