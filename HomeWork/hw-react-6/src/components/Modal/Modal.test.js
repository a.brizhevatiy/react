import React from "react";
import { Modal } from "./Modal";
import { render } from "@testing-library/react";

describe("Modal testing", () => {
    test("Smoke testing", () => {
        render(<Modal />);
    });
    test("Show Modal", () => {
        const openModal = true;
        render(<Modal modalIsOpen={openModal} />);
        expect(
            document.getElementsByClassName("modalOverlay")[0]
        ).not.toBeUndefined();
    });
    test("Do Not Show Modal", () => {
        const openModal = false;
        render(<Modal modalIsOpen={openModal} />);
        expect(
            document.getElementsByClassName("modalOverlay")[0]
        ).toBeUndefined();
    });
    test("Show prop 'header' in body", () => {
        const testHeader = "Some cool header";
        render(<Modal header={testHeader} modalIsOpen={true} />);
        const header = document.getElementsByClassName("modalTitle")[0];
        expect(header.textContent).toBe(testHeader);
    });

    test("Show prop 'text' in body", () => {
        const testText = "Some cool text";
        render(<Modal text={testText} modalIsOpen={true} />);
        const text = document.getElementsByClassName("modalBody")[0];
        expect(text.textContent).toBe(testText);
    });

    test("Show close button", () => {
        const closeButton = true;
        render(<Modal modalIsOpen={true} closeButton={closeButton} />);
        expect(
            document.getElementsByClassName("modalCloseButton")[0]
        ).not.toBeUndefined();
    });
    test("Do not show close button", () => {
        const closeButton = false;
        render(<Modal modalIsOpen={true} closeButton={closeButton} />);
        expect(
            document.getElementsByClassName("modalCloseButton")[0]
        ).toBeUndefined();
    });
});
