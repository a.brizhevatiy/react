import { Formik, Form } from "formik";
import React from "react";
import "./CartFormFormik.scss";
import FormSchema from "./FormSchema";
import { saveFormData, showForm, userReady } from "../../store/Form/actions";
import { connect } from "react-redux";
import { changeCart } from "../../store/App/actions";
import FormField from "./FormField";

const CartFormFormik = ({
    saveFormData,
    showForm,
    cart,
    userReady,
    changeCart,
}) => {
    const validSchema = FormSchema;

    return (
        <div>
            <Formik
                initialValues={{
                    name: "",
                    secondName: "",
                    userAge: "",
                    userLocation: "",
                    userPhoneNumber: "",
                    email: "",
                }}
                validateOnBlur
                onSubmit={(values, actions) => {
                    saveFormData(values);
                    userReady(true);
                    console.log("User: ", values, "Goods: ", cart);
                    actions.resetForm();
                    showForm(false);
                    changeCart([]);
                }}
                validationSchema={validSchema}
            >
                {({
                    isValid,
                    dirty,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                }) => (
                    <Form className="myForm">

                        <FormField
                            label="Имя"
                            name="name"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Your First Name"
                        />

                        <FormField
                            label="Фамилия"
                            name="secondName"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Your Second Name"
                        />

                        <FormField
                            label="Возраст"
                            name="userAge"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Your Age"
                            type="number"
                        />

                        <FormField
                            label="Адрес"
                            name="userLocation"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Your Adress"
                            type="textarea"
                            className="myForm__text-area"
                            as="textarea"
                        />

                        <FormField
                            label="Телефон"
                            name="userPhoneNumber"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Your Phone"
                        />

                        <FormField
                            label="Почта"
                            name="email"
                            onChange={handleChange}
                            onBlur={handleBlur}
                            placeholder="Your Email"
                        />

                        <button
                            disabled={!isValid && !dirty}
                            onClick={handleSubmit}
                            type={"submit"}
                        >
                            Отправить
                        </button>
                        <button
                            type="button"
                            onClick={() => {
                                showForm(false);
                            }}
                        >
                            Закрыть
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

const mapStateToProps = (state) => {
    return { userIsReady: state.sform.userIsReady };
};

const mapDispatchToProps = {
    saveFormData,
    showForm,
    userReady,
    changeCart,
};

export default connect(mapStateToProps, mapDispatchToProps)(CartFormFormik);
