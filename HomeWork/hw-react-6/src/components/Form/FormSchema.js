import * as Yup from "yup";

// Имя пользователя
// Фамилия пользователя
// Возраст пользователя
// Адрес доставки
// Мобильный телефон
const phoneRegExp = /^((\+38))?([ ])?((\(\d{3}\))|(\d{3}))?([ ])?(\d{3}[- ]?\d{2}[- ]?\d{2})$/

const FormSchema = Yup.object().shape({
    name: Yup.string().max(15, "Не более 15 букв").required("Обязательное поле"),

    secondName: Yup.string().max(15, "Не более 15 букв").required("Обязательное поле"),

    userAge: Yup.number()
        .min(18, "А 18 исполнилось?")
        .integer("Цифрами, пожалуйста")
        .required("Обязательное поле"),

    userLocation: Yup.string("Место для текста").required("Обязательное поле"),

    userPhoneNumber: Yup.string()
        .required("Обязательное поле")
        .matches(phoneRegExp, 'Номер введен неверно'),

    email: Yup.string()
        .email("Неверный формат почты")
        .typeError("Строка!")
        .required("Обязательное поле"),
});

export default FormSchema;
