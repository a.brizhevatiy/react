import React from 'react';
import PropTypes from "prop-types";
import {connect} from "react-redux";

function Favorites({favorites}) {
    return (
        <div>
            В Избранном: {favorites.length}
        </div>
    );
}

Favorites.propTypes = {
    items: PropTypes.array,
};

Favorites.defaultProps = {
    items: [],
};
const mapStateToProps = (state) => {
    return {
        favorites: state.app.favorites,
    };
};

export default connect(mapStateToProps)(Favorites);