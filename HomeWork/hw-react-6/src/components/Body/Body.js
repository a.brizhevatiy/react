import React from 'react';
import Cards from "../Cards/Cards";
import PropTypes from "prop-types";
import './Body.scss'
import {connect} from "react-redux";

function Body({items}) {
    return (
        <div className='body'>
            <Cards
                items={items}
            />
        </div>
    );
}

Body.propTypes = {
    items: PropTypes.array,
};

Body.defaultProps = {
    items: [],
};

const mapStateToProps = (state) => {
    return {
        items: state.app.items.itemsList,
    }
}

export default connect(mapStateToProps)(Body);