import React from 'react';
import {Route, Switch} from 'react-router-dom';
import FavoritesPage from "../pages/FavoritesPage/FavoritesPage";
import CartPage from "../pages/CartPage/CartPage";
import Body from "../components/Body/Body";
import page404 from "../pages/page404/page404";

function Routes() {
    return (
        <>
            <Switch>
                <Route exact path='/' component={Body}/>
                <Route exact path='/favorites' component={FavoritesPage}/>
                <Route exact path='/cart' component={CartPage}/>
                <Route path='*' component={page404}/>
            </Switch>
        </>
    );
}

export default Routes;