import {

    SAVE_FORM_DATA,
    SHOW_FORM,
    USER_READY,
} from "./actions";

const defaultState = {
    formData: {},
    formIsShow: false,
    userIsReady: false,
};

export const myFormReducer = (state = defaultState, action) => {
    switch (action.type) {
        case SAVE_FORM_DATA:
            return { ...state, formData: action.payload };
        case USER_READY:
            return { ...state, userIsReady: action.payload };
        case SHOW_FORM:
            return { ...state, formIsShow: action.payload };
        default:
            return state;
    }
};
