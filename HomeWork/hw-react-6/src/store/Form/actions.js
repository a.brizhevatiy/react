
export const SAVE_FORM_DATA = "SAVE_FORM_DATA";
export const SHOW_FORM = "SHOW_FORM";
export const USER_READY = "USER_READY";


export const saveFormData = (value) => (dispatch) => {
    dispatch({
        type: SAVE_FORM_DATA,
        payload: value,
    });
};
export const showForm = (value) => (dispatch) => {
    dispatch({
        type: SHOW_FORM,
        payload: value,
    });
};

export const userReady = (value) => (dispatch) => {
    dispatch({
        type: USER_READY,
        payload: value,
    });
};
