import React from "react";
import Cards from "../../components/Cards/Cards";
import PropTypes from "prop-types";
import "./CartPage.scss";
import { connect } from "react-redux";
import CartFormFormik from "../../components/Form/CartFormFormik";
import { showForm} from "../../store/Form/actions";

function CartPage({ items, cart, formIsShow, showForm, showReduxForm }) {
    const cartItems = items.filter((item) => cart.indexOf(item.id) !== -1);

    function formButton(){
        if (cartItems.length){
            showForm(true);
        } else {
            alert('Добавьте товары в корзину.')
        }
    }

    return (
        <>          
            <button className="cart__buyButton" onClick={formButton}>
                Оформить покупку Formik
            </button>
            {!cart.length && (
                <div className={"cart__empty"}>
                    В Корзину ничего не добавлено... :`(
                </div>
            )}

            {!!cart.length && (
                <div className={"cart"}>
                    <Cards items={cartItems}  />
                </div>
            )}

            {formIsShow && <CartFormFormik cart={cartItems} />}
        </>
    );
}

CartPage.propTypes = {
    items: PropTypes.array,
    cart: PropTypes.array,
};

CartPage.defaultProps = {
    items: [],
    cart: [],
};
const mapStateToProps = (state) => {
    return {
        items: state.app.items.itemsList,
        cart: state.app.cart,
        formIsShow: state.sform.formIsShow,
    };
};

const mapDispatchToProps = {
    showForm,
};

export default connect(mapStateToProps, mapDispatchToProps)(CartPage);
