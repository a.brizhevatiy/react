import React, {PureComponent} from 'react';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import './App.css';

class App extends PureComponent {
    state = {
        isOpenFirst: false,
        isOpenSecond: false,
    }

    render() {
        const {isOpenFirst, isOpenSecond} = this.state;
        return (
            <div className="App">
                <Button onClick={() => {
                    this.openModal('isOpenFirst')
                }}
                        text={'Open first modal'}
                        className={'button'}
                        backgroundColor={'grey'}/>

                <Button onClick={() => {
                    this.openModal('isOpenSecond')
                }}
                        text={'Open second modal'}
                        className={'button'}
                        backgroundColor={'grey'}/>

                <Modal header={'Do you want to delete this file?'}
                       closeButton={true}
                       text={'Once you delete this file, it won’t be possible to undo this action. \n' +
                       'Are you sure you want to delete it?'}
                       isOpen={isOpenFirst}
                       closeFunc={this.cancelButtonFunction}>

                    <Button text={'OK'}
                            onClick={this.okButtonFunction}
                            className={'button'}
                            backgroundColor={'rgba(0, 0, 0, 0.2)'}/>

                    <Button text={'Cancel'}
                            onClick={this.cancelButtonFunction}
                            className={'button'}
                            backgroundColor={'rgba(0, 0, 0, 0.2)'}/>
                </Modal>
                <Modal header={'Some other head'}
                       text={'Some veeeeeerrrrryyyyyyyy verrrryyyyyyyyyyyyyyyyyyyy so ' +
                       'verrrrrrrrrrrrrrrrrrrrrrrrrrrrryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy cool text'}
                       isOpen={isOpenSecond}
                       closeFunc={this.cancelButtonFunction}>

                    <Button text={'Подтвердить'}
                            onClick={this.okOtherButtonFunction}
                            className={'button'}
                            backgroundColor={'black'}/>

                    <Button text={'Отмена'}
                            onClick={this.cancelButtonFunction}
                            className={'button'}
                            backgroundColor={'black'}/>
                </Modal>
            </div>
        );
    }


    okButtonFunction = () => {
        console.log('Что то сделал')
        this.setState({isOpenFirst: false});
    }

    okOtherButtonFunction = () => {
        console.log('Что то другое сделал')
        this.setState({isOpenSecond: false});
    }
    cancelButtonFunction = () => {
        console.log('Закрыл')
        this.setState({isOpenFirst: false, isOpenSecond: false});
    }

    openModal = (key) => {
        this.setState({[key]: true})
    }
}

export default App;